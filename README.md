# webssh

#### 介绍
利用ssh2实现的webssh，项目来源于**xftx**的[webssh项目-只有app文件夹是项目工程代码,整个项目就是node项目，包含web前后端](https://gitee.com/xftx/webssh2?_from=gitee_search)。
本文代码内容就是该项目app文件目录下的内容。

#### 部署过程
1. `git clone https://gitee.com/nut77/webssh.git`
2. `cd webssh`
3. `npm install`
4. `npm audit fix`  可能会提示操作这一步
5. `node index.js` 或 `npm start` 项目启动

#### 使用说明
1. 启动成功后页面访问 `http://localhost:2222/ssh/host/需要连接的服务器IP`
2. 输入**需要连接的服务器IP**的用户和密码成功后
3. 出现shell终端
![webssh界面](img/img.png)
   
#### 参考地址
- [相关博客](https://blog.frytea.com/archives/33/)